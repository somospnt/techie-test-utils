package com.somospnt.techietestutils.repository;

import com.somospnt.techietestutils.domain.Squishy;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Repository
public class SquishyRepository {

    @Value("${com.techie.uri}")
    private String uri;
    @Autowired
    private RestTemplate restTemplate;

    public Optional<Squishy> obtenerPorNombre(String nombre) {
        try {
            return Optional.of(restTemplate.getForObject(uri + "/q?nombre=" + nombre, Squishy.class));
        } catch (HttpClientErrorException ex) {
            return Optional.empty();
        }
    }

}
